import axios from 'axios';
import React, { useState } from 'react'

function Profile({dataAccount}) {
  const [info,setInfo] = useState();
  const profiledataJSON = async() => {
    const resp = await axios.get('https://yzkbackend.herokuapp.com/account/myAccount');
    try {
      setInfo(resp.data.updatedAt);
    }catch(error) {
      alert(error);
    }
  }
  profiledataJSON();
    return (
      
    <div className="container">
    <h1>Edit Profile</h1>
  	<hr/>
	<div className="row">
      <div className="col-md-3"> 
        <div className="text-center">
          <img src="//placehold.it/100" className="avatar rounded-circle" alt="avatar"/>
          <h6>Upload a different photo...</h6>
          <input type="file" className="form-control"/>
        </div>
      </div>
      <div className="col-md-9 personal-info">
        <div className="alert alert-info alert-dismissable">
          <a href='/'className="panel-close close" data-dismiss="alert">×</a> 
          <i className="fa fa-coffee"></i>
         <div className="col"> Sist oppdatert : {info ? new Date(info).toUTCString() : 'Fetching...'}</div>
        </div>
        <h3>Personal info</h3>
        <form className="form-horizontal" method='POST'>
        <div className="form-group">
            <label className="col-lg-3 control-label">E-Post</label>
            <div className="col-lg-8">
              <input className="form-control" type="text" placeholder={dataAccount ? dataAccount.epost : 'Loading...'} disabled/>
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3">Brukernavn</label>
            <div className="col-lg-8">
              <input className="form-control" type="text" placeholder={dataAccount ? dataAccount.brukernavn : 'Loading..'}/>
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3">Passord</label>
            <div className="col-lg-8">
              <input className="form-control" type="password"/>
            </div>
          </div>
          <div className="form-group">
            <label className="col-lg-3">Bekreft Passord</label>
            <div className="col-lg-8">
              <input className="form-control" type="password"/>
              <br></br>
              <button type="button" className="btn btn-dark">Endre</button>
            </div>
            
          </div>
       
        </form>
      </div>
      
      </div>
      </div>
    )
}

export default Profile
