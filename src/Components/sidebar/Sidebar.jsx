import React from 'react'
import './sidebar.css'

function Sidebar() {
    return (
        <div className="sidebar">
            <span className="sidebarTitle">Om meg</span>

            <div className="sideBarItem">
                <img src="https://via.placeholder.com/300.png" alt="myimage" />
            </div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente aperiam, iste ipsam consectetur possimus asperiores sunt nisi nesciunt nemo, quos doloremque quasi temporibus, rerum rem dolorem? Mollitia unde nisi temporibus!</p>
            <br></br>
            <div className="sideBarItem">
            <span className="sidebarTitle">Kategorier
            <ul className="sidebarList">
                <li className="sideBarListItem">Sports</li>
                <li className="sideBarListItem">E-Sports</li>
                <li className="sideBarListItem">Tech</li>
            </ul>
            </span>

            </div>
            <div className="sidebarTitle">Følg meg!
            <div className="sidebarSocial">
            <i className="iconContact fab fa-facebook-square">
            </i>
            <i className="iconContact fab fa-linkedin"></i>
            </div>
            </div>

        </div>
        
    )
}

export default Sidebar
