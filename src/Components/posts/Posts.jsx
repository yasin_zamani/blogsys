import './posts.css'
import Post from './../post/Post'
import axios from 'axios';

function Posts({data}) {
  
    return (
        <>
        <div className="posts">
          <Post innlegg={data}/>
        </div>
        </>
    )
}

export default Posts
