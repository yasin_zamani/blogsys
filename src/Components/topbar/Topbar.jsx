import React from 'react'
import Cookies from 'universal-cookie';
const cookies = new Cookies();
function Topbar() {
const x = cookies.get("jwt2");
let user = false;
if(x) {
  user = true;
}
    return (  
  <nav className="navbar navbar-expand-lg navbar-light bg-dark size topnav-centered ">
  <button className="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <a className="navbar-brand" href="https://www.facebook.com/yzk1337/">
    <i className="topLeftIcon fab fa-facebook-square"></i>
        </a>
  <div className="collapse navbar-collapse " id="navbarNavDropdown">
    <ul className="navbar-nav topCenter">
      <li className="nav-item active">
        <a className="nav-link text-light" href="/">Hjem<span className="sr-only"></span></a>
      </li>
      <li className="nav-item">
      {user? true: <a className="nav-link text-light" href="/Login">Login</a>}
      </li>
      <li className="nav-item">
      {user? true: <a className="nav-link text-light" href="/Register"> Register</a>}
      </li>
      <li className="nav-item">
        <a className="nav-link text-light" href="/skriv">Skriv ny blog</a>
      </li>
      <li className="nav-item">
       {!user? true: <a className="nav-link text-light" href="/konto">Min Konto</a>}
      </li>
      <li className="nav-item">
       {!user? true: <a className="nav-link text-light" href="/logut">Log ut</a>}
      </li>
    </ul>
  </div>
  
</nav>
    )
}

export default Topbar
