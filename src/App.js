import Topbar from './Components/topbar/Topbar';
import Home from '././sider/Home/Home'
import Single from '././sider/single/Single'
import Write from '././sider/Write/Write'
import Register from './sider/register/Register'
import Login from './sider/login/Login'
import Profile from './sider/profile/Profile'
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Cookies from 'universal-cookie';
import axios from 'axios';
import { useState, useEffect } from 'react';
const cookies = new Cookies();

function App() {
const [name,setName] = useState();
const [user,setUser] = useState(false);
const [dataUser,setDataUser] = useState();
useEffect(()=> {
const x = cookies.get("jwt2");
const verifyToken= async () =>{
  const response =  await axios.get('https://yzkbackend.herokuapp.com/account/verifyJWT/'+x);
  try {
    setName(response.data.brukernavn);
    if(response.status === 200) {
      setUser(true);
      setDataUser(response.data);
    } else {
      setUser(false);
    }
  }catch(error) {
    console.log(error);
  }
}
if(cookies.get('jwt2')){
  verifyToken();
}
},[])

  return (
    <div className="App">
      <Router>
      <Topbar/>
      <Switch>
        <Route exact path="/">
          <Home data={name}/>
        </Route>
        <Route path="/skriv">
        {user ? <Write dataAccount={dataUser}/> : <Register/>}
        </Route>
        <Route path="/Register">
          {user ? <Home/> : <Register/>}
        </Route>
        <Route path="/Login">
        {user ? <Home/> : <Login/>}
        </Route>
        <Route path="/konto">
          {user ? <Profile dataAccount={dataUser}/> : <Login/> }
        </Route>
        <Route path="/post/:id">
        <Single/>
        </Route>
      </Switch>
      </Router>
     
     
    </div>
    
  );
}

export default App;
