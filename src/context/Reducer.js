const LoginReducer =(state,action) => {
    switch(action.type) {
        case "LOGIN_START":
            return {
                bruker:null,
                isFetching:true,
                erorr:false
            }
        case "LOGIN_SUCCESS":
            return {
                bruker:action.payload,
                isFetching:false,
                error:false
            }
        case "LOGIN_FAILED":
            return {
                bruker:null,
                isFetching:false,
                error:true
            }
        default:
            return state;
             
    }
}

export default LoginReducer;
