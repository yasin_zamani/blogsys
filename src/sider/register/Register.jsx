import {React, Component} from 'react'
import './register.css'
import axios from 'axios';
axios.defaults.withCredentials = true;

class Register extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             brukernavn: '',
             epost: '',
             passord: ''
        }

    }
    ChangeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }
    submitHandler = (e) => {
        
        e.preventDefault();
        axios.post('https://yzkbackend.herokuapp.com/account/register',this.state)
        .then(response => {
           console.log(response.data);
           const USER_TOKEN = response.data;
          
           axios.get('http://localhost:3001/',  {withCredentials: true})
            .then(response => {
                // If request is good...
                console.log(response.data);
                localStorage.setItem('USER_TOKEN', USER_TOKEN);
                
                
             })
            .catch((error) => {
                console.log('error ' + error);
             });
         })   
        .catch((error) => {
           console.log('error ' + error);   
        });

        

    }

    
    
    render() {
        const  {brukernavn,epost,passord}  = this.state;
        return (
            <>
            <div className='d-flex justify-content-center align-items-center container Register'>
            <div className="container border">
            <form onSubmit={this.submitHandler}> 
            <div className="form-group row d-flex justify-content-center align-items-center">
                <div className="col-sm-4">
                <label htmlFor="navn">Blogg Brukernavn</label>
            <input type="text" className="form-control" name="brukernavn" id="navn" aria-describedby="Brukernavn" placeholder="Skriv brukernavn" value={brukernavn} onChange={this.ChangeHandler} ></input>
            <small id="Brukernavn" className="form-text text-muted">Brukernavn er synlig for alle</small>
                </div>
            </div>
            <div className="form-group row d-flex justify-content-center align-items-center">
                <div className="col-sm-4 ">
                <label htmlFor="epost">Epost</label>
            <input type="email" className="form-control" name="epost" id="epost" aria-describedby="emailHelp" placeholder="Epost addressen" value={epost} onChange={this.ChangeHandler}></input>
                </div>
            </div>
            <div className="form-group row d-flex justify-content-center align-items-center">
                <div className="col-sm-4">
                <label htmlFor="Passord1">Passord</label>
            <input type="password" className="form-control" name="passord" id="passord" aria-describedby="emailHelp" placeholder="Passord" value={passord} onChange={this.ChangeHandler}></input>
                </div>
        </div>    
        <div className="col text-center">
      <button type="submit" className="btn btn-default">Register</button>
        </div>
            </form>
            </div>
        </div>

        <div className="container">
            <div className ="row">
                <div className="col">

        
                </div>
            </div>
        </div>
           </>
        )
    }
}

export default Register
