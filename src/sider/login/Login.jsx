import axios from 'axios';
import {React,useState,useContext} from 'react'
import Cookies from 'universal-cookie';
import { Context } from '../../context/Context';
const cookies = new Cookies();
axios.defaults.withCredentials = true;
function Login() {
    const [error,setError] = useState('');
    const [data, setData] = useState({
        brukernavn: '',
        passord: '',
    });
    const {bruker, stateDispatch,isFetching} = useContext(Context)

    const ChangeHandler = (e) => {
        setData({...data,[e.target.name]:e.target.value})
    }
    const {username,password} = data;
    const onSubmitHanlder = async (e) => {
        e.preventDefault();
        stateDispatch({type:"LOGIN_START"})
        try {
            const resp = await axios.post('https://yzkbackend.herokuapp.com/account/login', data);
            if(resp.status === 202) {
                setError(resp.data)
            } else if(resp.status === 200) {
                let d = new Date();
                d.setTime(d.getTime() + (120*60*1000));
                cookies.set("jwt2", resp.data.tokenID,{expires: d});
                window.location.href ="/";
                await stateDispatch({type:"LOGIN_SUCCESS",payload: resp.data})
                console.log(bruker);
                console.log(isFetching);
            }
        } catch(error) {
            stateDispatch({type:"LOGIN_FAILED"})
        }
        
    }
    
    return (
        <>
        <h2 className='text'>Login til Systemet</h2>
        <div className='d-flex justify-content-center align-items-center container Register'>
            <div className="container border">
            <form onSubmit={onSubmitHanlder}>
            <div className="form-group row d-flex justify-content-center align-items-center">
                <div className="col-sm-4">
                <label htmlFor="brukernavn">Blogg Brukernavn</label>
            <input type="text" className="form-control" name="brukernavn" aria-describedby="Brukernavn" placeholder="Skriv brukernavn" value={username} onChange={ChangeHandler} ></input>
            <small id="Brukernavn" className="form-text text-muted">Brukernavn er synlig for alle</small>
                </div>
            </div>
            <div className="form-group row d-flex justify-content-center align-items-center">
                <div className="col-sm-4">
                <label htmlFor="passord">Passord</label>
            <input type="password" className="form-control" name="passord" aria-describedby="emailHelp" placeholder="Passord" value={password} onChange={ChangeHandler}></input>
                </div>
        </div>    
        <div class="col text-center">
      <button class="btn btn-default">Login</button>
      <p className="text-warning">{error}</p>
        </div>
            </form>
            </div>
        </div>
        </>
        
    )
}

export default Login
