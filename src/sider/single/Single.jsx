import React from 'react'
import './single.css';
import Sidebar from './../../Components/sidebar/Sidebar';
import SinglePost from './../../Components/singlepost/Singlepost';

function Single() {
    return (
        
        <div className="single">
            <SinglePost/>
            <Sidebar/>
        </div>
      
    )
}

export default Single
