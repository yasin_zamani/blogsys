import './home.css'
import {React,useEffect,useState} from "react";
import {Link} from 'react-router-dom';
import Header from "../../Components/header/Header"
import axios from "axios";
import {Card, CardContent, Typography, Grid,Container,} from '@material-ui/core';
function Home({data}) {
    const [postdata,setData] = useState([]);
    useEffect(()=> {
       const getData = async ()  => {
           const response = await axios.get("http://yzkbackend.herokuapp.com/post/allPosts");
           try {
               setData(response.data);
           } catch (error) {
               console.log(error);
           }
        }
        getData();
    },[setData])

    return (
        <div className="home ">
            <Header data1={data}/>
            <div>
        <Container>
          <Typography
            color="textPrimary"
            gutterBottom
            variant="h4"
          > Latest blog written.. why not write your own?
          </Typography>
          <Grid container spacing={5}
         >
            {postdata.map((electronic) => (
              <Grid item xs={12} sm={5} key={electronic._id}>
                <Card>
                    <Link to={`/post/${electronic._id}`}>
                    <img src="https://images.unsplash.com/photo-1623706294103-e6a66ef09201?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib" alt="" width="250" height="150"/>
                    </Link>
                  <CardContent>
                  <Link to={`/post/${electronic._id}`}>
                    <Typography color="primary" variant="h5">
                      {electronic.title}
                    </Typography>
                    </Link>
                    <Typography color="secondary" variant="text" className="postDescription">
                      {electronic.innhold}
                    </Typography>
                    <Typography color="textSecondary" variant="subtitle2">
                      {electronic.brukernavn}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Container>
      </div>
        
        </div>
        
    )
}

export default Home
