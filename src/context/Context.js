import { createContext,useReducer } from "react";
import LoginReducer from './Reducer';
const INITIAL_STATE ={
    bruker:null,
    isFetching:false,
    error:false
}

export const Context = createContext(INITIAL_STATE);

export const ContextProvider = ({children}) => {
    const [state,stateDispatch] = useReducer(LoginReducer, INITIAL_STATE);
    return (<Context.Provider value={{bruker:state.bruker, isFetching:state.isFetching, error:state.error, stateDispatch}}>{children}</Context.Provider>)
}