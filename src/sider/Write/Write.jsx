import axios from 'axios';
import {React, useState,useEffect} from 'react'
import './write.css'



function Write({dataAccount}) {
    const [data, setData] = useState({
        title: '',
        innhold: '',
        brukernavn: ''
    });
    const eventHandler = (e) => {
        data.brukernavn = dataAccount.brukernavn;
        setData({...data,[e.target.name]:e.target.value})
    }

    const submitHandler = async (e) => {
        e.preventDefault();
        const response = await axios.post('http://yzkbackend.herokuapp.com/post/',data);
        try {
            console.log(response);
        }catch(error){
            console.log(error);
        }
    }
    return (
        <div className="write">
            <img className="imageHolder" src="https://via.placeholder.com/720" alt="blogimage" />
           <form className ="formWrite" onSubmit={submitHandler}>
               <div className="writeGroup">
                   <label htmlFor='fileImage'>
                       <p>Bilde til blog</p>
                   <i className="ikon fas fa-plus"></i>
                   </label>
                   <input className='file' type="file" id="fileImage"/>
                   <input className='inputText'type="text" name="title" id="titleName" placeholder='Blog title' value={data ? data.title : ''} onChange={eventHandler}autoFocus={true}/>
                   <div className="inputText">
               <textarea className="inputTxt" name="innhold" placeholder='Skriv om bloggen ...' onChange={eventHandler}></textarea>
               <p hidden id="brukernavn" name="brukernavn">{dataAccount ? dataAccount.brukernavn : 'Loading..'}</p>
           </div>
               </div>
               <div><button className='submitbtn'>Sende inn</button></div>
           </form>
        </div>
    )
}

export default Write
