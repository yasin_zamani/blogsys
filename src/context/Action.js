export const LoginStart = (brukerInfo) => ({
    type: "LOGIN_START"
})

export const LoginSuccess = (bruker) => ({
    type: "LOGIN_SUCCESS",
    payload:brukerInfo
})

export const LoginFailed = () => ({
    type: "LOGIN_FAILED"
})