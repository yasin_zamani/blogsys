import {React,useState,useEffect,useContext} from 'react'
import './singlepost.css'
import axios from 'axios';
import { Context } from '../../context/Context';
function Singlepost() {
    const [singleData, setData] = useState();
    const [Loading, setLoading] = useState(false)
    const [errormsg,setError] = useState('');
    const urlIndex = window.location.href.split('/')[4];
    const {bruker,isFetching} = useContext(Context)
    
   async function handleClick(e) {
        e.preventDefault();
        const deleteReq = await axios.delete("https://yzkbackend.herokuapp.com/post/"+urlIndex,{
            headers: {
              jwt2 : 'nada'
            }
          });
        try {
            console.log(deleteReq);
            alert(deleteReq.data);
            if(deleteReq.data.length < "15") {
                window.location.href =  "/";
            }
        }catch(error) {
            console.log(error)
        }
      
    
      }
    useEffect(()=> {
       async function getdata() {
        
            const response = await axios.get("http://yzkbackend.herokuapp.com/post/"+urlIndex)
            try {
                if(response.data) {
                    setData(response.data)
                }
            }catch (error) {
                console.log(error);
            }
            setLoading(true);
        }
    
        getdata();
        
    },[]) 
    
    return (
    
    <div className="container">
    
        <div className="singlePost">
            <div className="singlePostwr">
                <img src="https://images.unsplash.com/photo-1623706294103-e6a66ef09201?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8" alt="postImg" className="singlePostIMG" />
                <h1 className="singlePostTitle">
                {Loading ? (singleData.title): 'Loading...'}
                { singleData.brukernavn === document.getElementById('forfatter').textContent.split(" ")[1] &&  <div className="SinglePostEditContainer">
                    <i className="Edit far fa-edit"></i>
                    <i className="Edit fas fa-minus-circle" onClick={handleClick}></i>
                    </div>   
                    }
                </h1>
                <div className="singlePostInfo">
                    <span className="author" id="forfatter">Forfatter: <b>{Loading ? (singleData.brukernavn): 'Loading...'}</b></span>
                    <span className="date"><b>{Loading ? new Date(singleData.createdAt).toLocaleDateString(): 'Loading...'}</b></span>
                </div>
                <p className="Description"> {Loading ? (singleData.innhold): 'Loading...'}
                </p>
                <p className="bg-danger">{errormsg}</p>
            </div>
        </div>
        </div>
    )
}

export default Singlepost
