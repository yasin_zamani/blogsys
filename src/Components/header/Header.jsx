import React from 'react'
import './header.css'

function Header({data1}) {
    return (
        <div className="header">
            <div className="headerTitle">
               { <span className="headerTitleliten">{data1 ? <p>Velkommen til blog systemet : {data1}</p> : 'NodeJS and ReactJS blog'}</span>}
                <span className="headerTitleStor">Blog</span>
            </div>
            <img className="headerImage" src="https://images.unsplash.com/photo-1623706294103-e6a66ef09201?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib" alt="imagePlaceHolder" />
        </div>
    )
}

export default Header
